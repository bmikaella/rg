package hr.fer.zemris

/**
 * Created by bmihaela.
 */
data class Dot(val x: Double, val y: Double) {
    companion object {
        @JvmStatic
        fun add(first: Dot, second: Dot): Dot {
            val x = first.x + second.x
            val y = first.y + second.y
            return Dot(x, y)
        }

        @JvmStatic
        fun minus(first: Dot, second: Dot): Dot {
            val x = first.x - second.x
            val y = first.y - second.y
            return Dot(x, y)
        }

        @JvmStatic
        fun multiply(first: Dot, multiplier: Double): Dot {
            val x = first.x * multiplier
            val y = first.y * multiplier
            return Dot(x, y)
        }


    }
}