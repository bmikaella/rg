package hr.fer.zemris

import hr.fer.zemris.run.Fight
import hr.fer.zemris.run.Fight.Companion.MAX_SIZE_NORMAL
import hr.fer.zemris.run.Fight.Companion.MIN_SIZE
import hr.fer.zemris.run.Fight.Companion.SQUARE_DEFAULT_SPEED
import hr.fer.zemris.run.Fight.Companion.SQUARE_SPEED_MAX_MULTIPLIER
import java.util.*

/**
 * Created by bmihaela.
 */
data class Square(var first: Dot,
                  var second: Dot,
                  var third: Dot,
                  var fourth: Dot,
                  val isAngry: Boolean = false,
                  var color: Color,
                  val side: Int = -1,
                  val direction: Dot? = null) {

    companion object {

        @JvmStatic
        val random: Random = Random()
        @JvmStatic
        val forward: Dot = Dot(0.0, SQUARE_DEFAULT_SPEED)
        @JvmStatic
        val backward: Dot = Dot(0.0, -SQUARE_DEFAULT_SPEED)
        @JvmStatic
        val left: Dot = Dot(-SQUARE_DEFAULT_SPEED, 0.0)
        @JvmStatic
        val right: Dot = Dot(SQUARE_DEFAULT_SPEED, 0.0)
        @JvmStatic
        fun generateANormalSquare(): Square {
            val length = Fight.random.nextDouble() * MAX_SIZE_NORMAL + MIN_SIZE
            val side = random.nextInt(4)
            val beginx: Double
            val beginy: Double
            val direction: Dot
            val speed = random.nextDouble() * SQUARE_SPEED_MAX_MULTIPLIER
            if (side == 0) {
                beginx = -1 - random.nextDouble() - length
                beginy = random.nextDouble() * 2 - 1
                direction = Dot.multiply(right, speed)
            } else if (side == 1) {
                beginx = random.nextDouble() * 2 - 1
                beginy = 1 + random.nextDouble() + length
                direction = Dot.multiply(backward, speed)
            } else if (side == 2) {
                beginx = 1 + random.nextDouble()
                beginy = random.nextDouble() * 2 - 1
                direction = Dot.multiply(left, speed)
            } else {
                beginx = random.nextDouble() * 2 - 1
                beginy = -1 - random.nextDouble()
                direction = Dot.multiply(forward, speed)
            }

            val firstDot = Dot(beginx, beginy)
            val secondDot = Dot(beginx + length, beginy)
            val thirdDot = Dot(beginx + length, beginy - length)
            val fourthDot = Dot(beginx, beginy - length)
            val color = Color(0.0, 0.3, 0.7)
            return Square(firstDot, secondDot, thirdDot, fourthDot, false, color, side, direction)
        }

        @JvmStatic
        fun aggravateASquare(): Square {
            val firstDot = Dot(-0.05, 0.0)
            val secondDot = Dot(0.0, 0.05)
            val thirdDot = Dot(0.05, 0.0)
            val fourthDot = Dot(0.0, -0.05)
            val color = Color(1.0, 0.0, 0.0)
            return Square(firstDot, secondDot, thirdDot, fourthDot, true, color)
        }

        @JvmStatic
        fun checkEncounter(current: Square, angrySquare: Square): Encounter {
            if (calculateIfDotInTriangle(angrySquare.third, current.second, current.first, current.fourth)) {
                return Encounter.SQUARE_KILLED
            } else if (calculateIfDotInTriangle(angrySquare.fourth, current.second, current.first, current.fourth)) {
                return Encounter.SQUARE_KILLED
            } else if (calculateIfDotInTriangle(angrySquare.first, current.second, current.fourth, current.third)) {
                return Encounter.SQUARE_KILLED
            } else if (calculateIfDotInTriangle(angrySquare.second, current.second, current.fourth, current.third)) {
                return Encounter.SQUARE_KILLED
            }

            if (calculateIfDotInTriangle(current.first, angrySquare.first, angrySquare.fourth, angrySquare.third)) {
                return Encounter.ANGRY_DIED
            } else if (calculateIfDotInTriangle(current.second, angrySquare.first, angrySquare.fourth, angrySquare.third)) {
                return Encounter.ANGRY_DIED
            } else if (calculateIfDotInTriangle(current.third, angrySquare.first, angrySquare.third, angrySquare.second)) {
                return Encounter.ANGRY_DIED
            } else if (calculateIfDotInTriangle(current.fourth, angrySquare.first, angrySquare.third, angrySquare.second)) {
                return Encounter.ANGRY_DIED
            }

            return Encounter.NOTHING_HAPPENED
        }

        private fun calculateIfDotInTriangle(p: Dot, p0: Dot, p1: Dot, p2: Dot): Boolean {
            val area = 0.5 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y)
            val s = 1 / (2 * area) * (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y)
            val t = 1 / (2 * area) * (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y)
            val z = 1 - s - t
            if (s > 0 && t > 0 && z > 0) {
                return true
            }
            return false
        }
    }

    fun moveSquare() {
        move(direction!!)
    }

    fun moveLeft() {
        move(left)
    }

    fun moveRight() {
        move(right)
    }

    private fun move(distance: Dot) {
        first = Dot.add(first, distance)
        second = Dot.add(second, distance)
        third = Dot.add(third, distance)
        fourth = Dot.add(fourth, distance)
    }

    fun moveForward() {
        move(forward)
    }

    fun moveBackward() {
        move(backward)
    }

    fun outOfScreen(): Boolean {
        when (side) {
            0 -> {
                return first.x > 1
            }
            1 -> {
                return first.y < -1
            }
            2 -> {
                return second.x < -1
            }
            3 -> {
                return third.y > 1
            }
        }
        return false
    }
}