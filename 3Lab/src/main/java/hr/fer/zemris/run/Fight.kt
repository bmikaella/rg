package hr.fer.zemris.run

import hr.fer.zemris.Area
import hr.fer.zemris.Square
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.util.*

/**
 * Created by bmihaela.
 */
class Fight {

    companion object {

        @JvmStatic
        val random: Random = Random()
        var POPULATION: Int = 20
        var rebirth = 3
        var MAX_SIZE_NORMAL: Double = 0.5
        var MIN_SIZE: Double = 0.1
        var SQUARE_SPEED_MAX_MULTIPLIER = 1.5
        var SQUARE_DEFAULT_SPEED = 0.015


        @JvmStatic
        fun main(args: Array<String>) {
            if(args.size > 0 ){
                if(args.size == 6){
                    try {
                        POPULATION = args.get(0).toInt()
                        if(POPULATION < 1){
                            print("Na početku je potrebno da postoji barem jedan plavi kvadrat!")
                            System.exit(1)
                        }
                        rebirth = args.get(1).toInt()
                        if(rebirth < 0){
                            print("Broj kvadrata koji se stvaraju treba biti pozitivni broj!")
                            System.exit(1)
                        }
                        MAX_SIZE_NORMAL = args.get(2).toDouble()
                        MIN_SIZE = args.get(3).toDouble()
                        SQUARE_SPEED_MAX_MULTIPLIER = args.get(4).toDouble()
                        SQUARE_DEFAULT_SPEED = args.get(5).toDouble()
                    }catch (ex : RuntimeException){
                        print("Molimo vas da unesete pravilno formatirane vrijednosti!")
                        System.exit(1)
                    }
                }else{
                    print("Potrebno je unjeti 6 parametara!")
                    System.exit(1)
                }
            }


            val angrySquare = Square.aggravateASquare()
            val bystanders = createPopulation(POPULATION)
            val fightArea = Area(angrySquare, bystanders, rebirth)
            fightArea.makePanel()
            fightArea.addCanvasListener(object : KeyAdapter() {
                override fun keyPressed(e: KeyEvent?) {
                    if (fightArea.isDead) {
                        return
                    }
                    if (e!!.keyCode == KeyEvent.VK_LEFT) {
                        angrySquare.moveLeft()
                        fightArea.calculateDeaths()
                        fightArea.redraw()
                    } else if (e.keyCode == KeyEvent.VK_RIGHT) {
                        angrySquare.moveRight()
                        fightArea.calculateDeaths()
                        fightArea.redraw()
                    } else if (e.keyCode == KeyEvent.VK_UP) {
                        angrySquare.moveForward()
                        fightArea.calculateDeaths()
                        fightArea.redraw()
                    } else if (e.keyCode == KeyEvent.VK_DOWN) {
                        angrySquare.moveBackward()
                        fightArea.calculateDeaths()
                        fightArea.redraw()
                    }
                }
            })
            while (true) {
                if (fightArea.killedAll()) {
                    Thread.sleep(500)
                    println("Congratulations You Won!")
                    System.exit(0)
                }
                fightArea.calculateDeaths()
                Thread.sleep(50)
                fightArea.getGoing()
                fightArea.redraw()
            }

        }

        @JvmStatic
        fun createPopulation(pop: Int): List<Square> {
            var i = 0
            val population = mutableListOf<Square>()
            while (i < pop) {
                population.add(Square.generateANormalSquare())
                i++
            }
            return population
        }
    }
}