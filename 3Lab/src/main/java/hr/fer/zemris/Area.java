package hr.fer.zemris;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import hr.fer.zemris.run.Fight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by bmihaela.
 */
public class Area {

	private GLCanvas glCanvas;
	private JFrame jFrame;
	private Square angrySquare;
	private boolean isAngrySquareDead = false;
	private int rebirthCount;
	private List<Square> bystanders = new ArrayList<>();
	private int killed = 0;
	private int children = 0;
	private Boolean monitor = false;

	public Area(Square angrySquare, java.util.List<Square> bystanders, int rebirthCount) {
		this.angrySquare = angrySquare;
		this.bystanders.addAll(bystanders);
		this.rebirthCount = rebirthCount;
	}

	public void redraw() throws InterruptedException {
		glCanvas.display();
		synchronized (monitor) {
			bystanders.addAll(Fight.createPopulation(children));
		}
		children = 0;
	}

	public boolean killedAll() {
		synchronized (monitor) {
			return bystanders.size() == 0 && children == 0;
		}
	}

	public void calculateDeaths() throws InterruptedException {
		synchronized (monitor) {
			Iterator<Square> iterator = bystanders.iterator();
			while (iterator.hasNext()) {
				Square current = iterator.next();
				Encounter event = Square.checkEncounter(current, angrySquare);
				if (event == Encounter.SQUARE_KILLED) {
					iterator.remove();
					killed++;
				} else if (event == Encounter.ANGRY_DIED) {
					isAngrySquareDead = true;
					Thread.sleep(500);
					System.out.println("Points: " + killed);
					System.exit(0);
				}
			}
		}
	}

	public void addCanvasListener(KeyAdapter listener) {
		glCanvas.addKeyListener(listener);
	}

	public void makePanel() throws InvocationTargetException, InterruptedException {
		SwingUtilities.invokeAndWait(() -> {
			GLProfile glProfile = GLProfile.getDefault();
			GLCapabilities glCapabilities = new GLCapabilities(glProfile);
			glCanvas = new GLCanvas(glCapabilities);

			glCanvas.addGLEventListener(new GLEventListener() {
				public void init(GLAutoDrawable glAutoDrawable) {
				}

				@Override
				public void dispose(GLAutoDrawable glAutoDrawable) {
				}

				@Override
				public void display(GLAutoDrawable glAutoDrawable) {
					GL2 gl2 = glAutoDrawable.getGL().getGL2();
					gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
					gl2.glLoadIdentity();

					gl2.glBegin(GL2.GL_POLYGON);
					gl2.glColor3d(angrySquare.getColor().getRed(), angrySquare.getColor().getGreen(),
							angrySquare.getColor().getBlue());

					gl2.glVertex2d(angrySquare.getFirst().getX(), angrySquare.getFirst().getY());
					gl2.glVertex2d(angrySquare.getSecond().getX(), angrySquare.getSecond().getY());
					gl2.glVertex2d(angrySquare.getThird().getX(), angrySquare.getThird().getY());
					gl2.glVertex2d(angrySquare.getFourth().getX(), angrySquare.getFourth().getY());
					gl2.glEnd();

					for (Square square : bystanders) {
						gl2.glBegin(GL2.GL_POLYGON);
						gl2.glColor3d(square.getColor().getRed(), square.getColor().getGreen(),
								square.getColor().getBlue());

						gl2.glVertex2d(square.getFirst().getX(), square.getFirst().getY());
						gl2.glVertex2d(square.getSecond().getX(), square.getSecond().getY());
						gl2.glVertex2d(square.getThird().getX(), square.getThird().getY());
						gl2.glVertex2d(square.getFourth().getX(), square.getFourth().getY());
						gl2.glEnd();
					}

				}

				@Override
				public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
					GL2 gl2 = glAutoDrawable.getGL().getGL2();
					gl2.glMatrixMode(GL2.GL_PROJECTION);
					gl2.glLoadIdentity();

					GLU glu = new GLU();

					gl2.glMatrixMode(GL2.GL_MODELVIEW);
					gl2.glLoadIdentity();
					gl2.glViewport(0, 0, width, height);
				}
			});

			jFrame = new JFrame("Area");
			jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			jFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					jFrame.dispose();
					System.exit(0);
				}
			});
			jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
			jFrame.setVisible(true);
			glCanvas.requestFocusInWindow();

		});
	}

	private void removeOutOfScreen() {
		Iterator<Square> iterator = bystanders.iterator();
		while (iterator.hasNext()) {
			Square current = iterator.next();
			if (current.outOfScreen()) {
				iterator.remove();
				children += rebirthCount;
			}
		}
	}

	public void getGoing() {
		synchronized (monitor) {
			for (Square square : bystanders) {
				square.moveSquare();
			}
			removeOutOfScreen();
		}
	}

	public boolean isDead() {
		return isAngrySquareDead;
	}
}
