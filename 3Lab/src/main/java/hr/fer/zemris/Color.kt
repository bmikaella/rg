package hr.fer.zemris

/**
 * Created by bmihaela.
 */
data class Color(val red: Double, val green: Double, val blue: Double)