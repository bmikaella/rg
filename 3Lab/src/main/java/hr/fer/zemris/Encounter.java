package hr.fer.zemris;

/**
 * Created by bmihaela.
 */
public enum Encounter {
	ANGRY_DIED,
	SQUARE_KILLED,
	NOTHING_HAPPENED
}
