import java.time.Instant
import java.util.*

/**
 * Created by bmihaela.
 */
class VerticalFallDisruptor : IDisruptor {
    val random = Random()

    companion object {
        const val FALLING_FORCE = 0.5f

        //5 seconds
        const val DEFROSTING = 0.99f;

    }

    override fun influence(flake: Flake) {
        flake.timeToLive = flake.timeToLive - Instant.now().minusMillis(flake.creationTime.toEpochMilli()).toEpochMilli()
        println("Flake time to live " + flake.timeToLive)
        println("Flake life expectancy"+ flake.lifeExpectancy)
        if(flake.timeToLive <= 0){
            return
        }
//        flake.flakeStats.size = flake.flakeStats.size * flake.timeToLive * 1f
//        flake.flakeStats.size = flake.flakeStats.size *
//                ((flake.timeToLive - Instant.now().minusMillis(flake.creationTime.toEpochMilli()).toEpochMilli()) / flake.timeToLive) * 1f
        println("Flake size: " + flake.flakeStats.size)
        flake.flakeStats.size = flake.flakeStats.size *
                ((flake.timeToLive)*1.0f / flake.lifeExpectancy)
//        println(a)
//        val b = flake.flakeStats.size *

//        flake.flakeStats.size = flake.flakeStats.size *
//                ((flake.timeToLive - Instant.now().minusMillis(flake.creationTime.toEpochMilli()).toEpochMilli()) / flake.timeToLive) * 1f
        flake.flakeStats.colorR = flake.flakeStats.colorR * DEFROSTING
        flake.flakeStats.colorG = flake.flakeStats.colorG * DEFROSTING
        flake.flakeStats.colorB = flake.flakeStats.colorB * DEFROSTING
        flake.flakeStats.y = flake.flakeStats.y - FALLING_FORCE * flake.flakeStats.size
    }
}