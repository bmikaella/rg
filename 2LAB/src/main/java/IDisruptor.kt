/**
 * Created by bmihaela.
 */
interface IDisruptor {
    fun influence(flake: Flake)
}