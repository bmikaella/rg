
import java.time.Instant
import java.util.*

/**
 * Created by bmihaela.
 */
class Flake(
        val creationTime: Instant,
        var flakeStats: FlakeStats,
        val id: Double,
        val lifeExpectancy: Long,
        var timeToLive: Long
) {

    companion object {
        @JvmStatic
        val random = Random()

        const val SET_TIME_OF_LIFE = 20000.0;
        const val COLOR_INTENSITY = 0.9f;
        const val MINIMUM_SIZE = 0.2f

        @JvmStatic
        fun generateNewFlake(): Flake {
            //zbog dubine, tezina nema utjecaj
            val colorSaturation = random.nextFloat()
            val size = random.nextFloat() * random.nextInt(10)+ MINIMUM_SIZE
//            println(size)
            val flakeStats = FlakeStats(size = size, x = (random.nextDouble().toFloat() * 2f - 1), y = random.nextFloat() * 0.25f + 1f,
                    colorR = random.nextFloat() * COLOR_INTENSITY,
                    colorG = random.nextFloat() * COLOR_INTENSITY,
                    colorB = random.nextFloat() * COLOR_INTENSITY)
                    //vece pahuljive duze ziv
            val timeToLive = (SET_TIME_OF_LIFE * size).toLong()
            return Flake(Instant.now(), flakeStats, random.nextDouble(), lifeExpectancy =  timeToLive, timeToLive = timeToLive)
        }
    }
}