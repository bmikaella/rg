/**
 * Created by bmihaela.
 */
class FlakeStats(
        var size: Float,
        var x: Float,
        var y: Float,
        var colorR: Float,
        var colorG: Float,
        var colorB: Float
) {}