import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by bmihaela.
 */
public class SnowWeather {

	private GLCanvas glCanvas;
	private JFrame jFrame;
	private SnowStats snowStats;

	public SnowWeather(SnowStats snowStats) {
		this.snowStats = snowStats;
	}

	public void redraw() {
		glCanvas.display();
	}


	public void addCanvasListener(MouseAdapter listener) {
		glCanvas.addMouseListener(listener);
	}

	public void makePanel() throws InvocationTargetException, InterruptedException {
		SwingUtilities.invokeAndWait(() -> {
			GLProfile glProfile = GLProfile.getDefault();
			GLCapabilities glCapabilities = new GLCapabilities(glProfile);
			glCanvas = new GLCanvas(glCapabilities);

			glCanvas.addGLEventListener(new GLEventListener() {
				@Override
				public void init(GLAutoDrawable glAutoDrawable) {
				}

				@Override
				public void dispose(GLAutoDrawable glAutoDrawable) {
				}

				@Override
				public void display(GLAutoDrawable glAutoDrawable) {
					GL2 gl2 = glAutoDrawable.getGL().getGL2();
					gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
					gl2.glLoadIdentity();


					java.util.List<FlakeStats> flakes = snowStats.setForecastSnow();
					for (FlakeStats flake : flakes) {
						gl2.glPointSize(flake.getSize());
						gl2.glBegin(GL.GL_POINTS);
						gl2.glColor3f(flake.getColorR(), flake.getColorG(), flake.getColorB());
						gl2.glVertex2f(flake.getX(), flake.getY());
						gl2.glEnd();

					}
				}

				@Override
				public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
					GL2 gl2 = glAutoDrawable.getGL().getGL2();
					gl2.glMatrixMode(GL2.GL_PROJECTION);
					gl2.glLoadIdentity();

					GLU glu = new GLU();

					gl2.glMatrixMode(GL2.GL_MODELVIEW);
					gl2.glLoadIdentity();
				}
			});

			jFrame = new JFrame("SnowWeather");
			jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			jFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					jFrame.dispose();
					System.exit(0);
				}
			});
			jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
			jFrame.setVisible(true);
			glCanvas.requestFocusInWindow();

		});
	}
}
