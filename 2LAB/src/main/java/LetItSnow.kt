
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

/**
 * Created by bmihaela.
 */
class LetItSnow {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val snowStats = SnowStats(VerticalFallDisruptor())
            val snowWeather = SnowWeather(snowStats)

            snowWeather.makePanel()
            snowWeather.addCanvasListener(object : MouseAdapter() {
                override fun mouseClicked(e: MouseEvent) {
                }
            })
            while (true) {
                Thread.sleep(100)
                snowWeather.redraw()
            }
        }
    }
}