import java.util.*

/**
 * Created by bmihaela.
 */
class SnowStats(
        val disruptor: VerticalFallDisruptor
) {
    val flakes: MutableList<Flake> = mutableListOf()
    val random = Random()

    companion object {
        const val FLAKES_MAX_COUNT: Int = 100
    }

    fun setForecastSnow(): List<FlakeStats> {
        flakes.addAll(magic())
        //move old snowflakes in time
        //generate new snowflakes
        val flakesStats = arrayListOf<FlakeStats>()
        val flakeIteratos = flakes.iterator()
//        println(flakes.size)
        while (flakeIteratos.hasNext()) {
            val flake = flakeIteratos.next()
            disruptor.influence(flake)
            if (flake.flakeStats.y < -1f || flake.timeToLive <= 0 || flake.flakeStats.size <= Flake.MINIMUM_SIZE) {
                flakeIteratos.remove()
                continue
            }
            flakesStats.add(flake.flakeStats)
        }
//        println(flakes.size)
        return flakesStats
    }

    private fun magic(): List<Flake> {
        val snowFlakesCount = random.nextInt(FLAKES_MAX_COUNT)
        val newFlakes = arrayListOf<Flake>()
        for (i in 0..snowFlakesCount) {
            val flake = Flake.generateNewFlake()
            newFlakes.add(flake)
        }
        return newFlakes
    }

}