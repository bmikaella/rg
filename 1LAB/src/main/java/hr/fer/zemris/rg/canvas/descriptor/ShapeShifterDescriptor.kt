package hr.fer.zemris.rg.canvas.descriptor

import hr.fer.zemris.rg.canvas.elements.tri_dimensional.Plane3D
import hr.fer.zemris.rg.canvas.elements.tri_dimensional.Point3D
import hr.fer.zemris.rg.canvas.elements.tri_dimensional.ShapeShifter
import java.nio.file.Files
import java.nio.file.Path

/**
 * Created by bmihaela.
 */
class ShapeShifterDescriptor(description: Path) {
    val points: MutableList<Point3D> = mutableListOf()
    val planesDescriptors: MutableList<Plane3DDescriptor> = mutableListOf()
    var center = Point3D(0f, 0f, 0f)

    init {
        loadDescriptor(description)
    }

    fun loadDescriptor(description: Path) {
        val lines = Files.readAllLines(description)
        var xSum = 0.0
        var ySum = 0.0
        var zSum = 0.0
        for (line in lines) {
            val elements = line.split(" ")
            if (line.startsWith("v")) {
                val xCoordinate = elements[1].toFloat()
                val yCoordinate = elements[2].toFloat()
                val zCoordinate = elements[3].toFloat()
                xSum += xCoordinate
                ySum += yCoordinate
                zSum += zCoordinate
                points.add(Point3D(xCoordinate, yCoordinate, zCoordinate))
            } else if (line.startsWith("f")) {
                planesDescriptors.add(Plane3DDescriptor(elements[1].toInt() - 1, elements[2].toInt() - 1, elements[3].toInt() - 1))
            }
        }
        center = Point3D((xSum/points.size).toFloat(), (ySum/points.size).toFloat(), (zSum/points.size).toFloat())
    }

    fun createNewShape(ratio: Float): ShapeShifter {
        val transformedPlanes = mutableListOf<Plane3D>()
        for (planeDescriptor in planesDescriptors) {
            val plane = Plane3D(points[planeDescriptor.first].scale(ratio), points[planeDescriptor.second].scale(ratio), points[planeDescriptor.third].scale(ratio))
            transformedPlanes.add(plane)
        }
        return ShapeShifter(transformedPlanes)
    }

}