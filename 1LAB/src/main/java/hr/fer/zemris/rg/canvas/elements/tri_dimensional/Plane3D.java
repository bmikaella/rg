package hr.fer.zemris.rg.canvas.elements.tri_dimensional;


import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.Objects;

/**
 * Created by bmihaela.
 */
public class Plane3D {

	private final Point3D firstPoint;
	private final Point3D secondPoint;
	private final Point3D thirdPoint;
	private final Float a;
	private final Float b;
	private final Float c;
	private final Float d;


	public Plane3D(Point3D first, Point3D second, Point3D third) {
		firstPoint = third;
		secondPoint = first;
		thirdPoint = second;

		a = (secondPoint.getYCoordinate()-firstPoint.getYCoordinate())*(thirdPoint.getZCoordinate()-firstPoint.getZCoordinate())
				-(secondPoint.getZCoordinate()-firstPoint.getZCoordinate())*(thirdPoint.getYCoordinate()-firstPoint.getYCoordinate());

		b = -(secondPoint.getXCoordinate()-firstPoint.getXCoordinate())*(thirdPoint.getZCoordinate()-firstPoint.getZCoordinate())
				+(secondPoint.getZCoordinate()-firstPoint.getZCoordinate())*(thirdPoint.getXCoordinate()-firstPoint.getXCoordinate());

		c = (secondPoint.getXCoordinate() - firstPoint.getXCoordinate())*(thirdPoint.getYCoordinate()-firstPoint.getYCoordinate())
				-(secondPoint.getYCoordinate()-firstPoint.getYCoordinate())*(thirdPoint.getXCoordinate()-firstPoint.getXCoordinate());

		d = -firstPoint.getXCoordinate()*a - firstPoint.getYCoordinate()*b - firstPoint.getZCoordinate()*c;
	}

	public Point3D getFirstPoint() {
		return firstPoint;
	}

	public Point3D getSecondPoint() {
		return secondPoint;
	}

	public Point3D getThirdPoint() {
		return thirdPoint;
	}

	public RealVector getNormal(){
		RealVector vector = new ArrayRealVector(4);
		double summedSquare = Math.sqrt(Math.pow(a,2.0)+Math.pow(b,2.0)+Math.pow(c,2.0)+Math.pow(d,2.0));
		vector.setEntry(0, a/summedSquare);
		vector.setEntry(1, b/summedSquare);
		vector.setEntry(2, c/summedSquare);
		vector.setEntry(3, d/summedSquare);
		return vector;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Plane3D plane3D = (Plane3D) o;
		return Objects.equals(firstPoint, plane3D.firstPoint) &&
				Objects.equals(secondPoint, plane3D.secondPoint) &&
				Objects.equals(thirdPoint, plane3D.thirdPoint);
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstPoint, secondPoint, thirdPoint);
	}
}
