package hr.fer.zemris.rg.canvas.elements.tri_dimensional

/**
 * Created by bmihaela.
 */
class BSpline {
    val segments = mutableListOf<MutableList<Point3D>>()
    val splineDots = mutableListOf<Point3D>()
    val tangents = mutableListOf<Line3D>()
    val directions = mutableListOf<Point3D>()
    var positionCount: Int = 0

    fun addSegment(segment: MutableList<Point3D>) {
        positionCount += segment.size
        segments.add(segment)
        splineDots.addAll(segment)
    }

    fun addTangents(tangents: MutableList<Line3D>) {
        this.tangents.addAll(tangents)
    }

    fun getTranslation(position: Int): Point3D {
        return splineDots.get(position)
    }

    fun getTangent(position: Int): Line3D {
        return tangents.get(position)
    }

    fun getDirection(position: Int): Point3D {
        return directions[position]
    }

    fun addDirections(directions: MutableList<Point3D>) {
        this.directions.addAll(directions)
    }
}