package hr.fer.zemris.rg.canvas.elements.tri_dimensional

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D
import org.apache.commons.math3.linear.MatrixUtils
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector

/**
 * Created by bmihaela.
 */

class Point3D(val xCoordinate: Float, val yCoordinate: Float, val zCoordinate: Float) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Point3D

        if (xCoordinate != other.xCoordinate) return false
        if (yCoordinate != other.yCoordinate) return false
        if (zCoordinate != other.zCoordinate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = xCoordinate.hashCode()
        result = 31 * result + yCoordinate.hashCode()
        result = 31 * result + zCoordinate.hashCode()
        return result
    }

    override fun toString(): String {
        return "Point3D(xCoordinate=$xCoordinate, yCoordinate=$yCoordinate, zCoordinate=$zCoordinate)"
    }

    fun getVector(secondPoint: Point3D): Vector3D {
        return Vector3D((secondPoint.xCoordinate - this.xCoordinate).toDouble(),
                (secondPoint.yCoordinate - this.yCoordinate).toDouble(),
                (secondPoint.zCoordinate - this.zCoordinate).toDouble())
    }

    fun getVector(secondPoint: RealVector): Vector3D {
        return Vector3D((secondPoint.getEntry(0) - this.xCoordinate),
                (secondPoint.getEntry(1) - this.yCoordinate),
                (secondPoint.getEntry(2) - this.zCoordinate))
    }

    fun getAsMatrix(): RealMatrix {
        return MatrixUtils.createRowRealMatrix(doubleArrayOf(xCoordinate.toDouble(), yCoordinate.toDouble(), zCoordinate.toDouble()))
    }

    fun getAsDoubleArray(): DoubleArray {
        return doubleArrayOf(xCoordinate.toDouble(), yCoordinate.toDouble(), zCoordinate.toDouble())
    }

    fun scale(ratio: Float): Point3D {
        return Point3D(xCoordinate * ratio, yCoordinate * ratio, zCoordinate * ratio)
    }

    fun add(point: Point3D): Point3D {
        return Point3D(this.xCoordinate + point.xCoordinate,
                this.yCoordinate + point.yCoordinate,
                this.zCoordinate + point.zCoordinate)

    }

    fun multiply(other: Point3D): Float {
        return this.xCoordinate * other.xCoordinate +
                this.yCoordinate * other.yCoordinate +
                this.zCoordinate * other.zCoordinate

    }

}