package hr.fer.zemris.rg.elements

import hr.fer.zemris.rg.canvas.Canvas
import hr.fer.zemris.rg.canvas.descriptor.BsplineDescriptor
import hr.fer.zemris.rg.canvas.descriptor.ShapeShifterDescriptor
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.nio.file.Paths

/**
 * Created by bmihaela.
 */
class BsplineCurve {
    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            val shapeShifterDescriptor = ShapeShifterDescriptor(Paths.get("src/main/resources/teddy.obj").toAbsolutePath().normalize())
            val bsplineDescriptor = BsplineDescriptor(Paths.get("src/main/resources/bspline").toAbsolutePath().normalize())

            val shapeData = shapeShifterDescriptor.createNewShape(0.2f)
            val bSpline = bsplineDescriptor.createNewBspline()

            val drawArea = Canvas(bSpline, shapeData)
            drawArea.makePanel(true)
            drawArea.addCanvasListener(object : KeyAdapter() {
                override fun keyPressed(e: KeyEvent?) {
                    if (e!!.keyCode == KeyEvent.VK_LEFT) {
                        drawArea.movePositionBackward()
                        drawArea.redraw()
                    } else if (e.keyCode == KeyEvent.VK_RIGHT) {
                        drawArea.movePositionForward()
                        drawArea.redraw()
                    }
                }
            })

            print("Automated ?[Y]")
            var line: String?
            do {
                line = readLine()
            } while (line!!.trim() != "Y")

            while (true) {
                Thread.sleep(10)
                drawArea.movePositionForward()
                drawArea.redraw()
            }
        }
    }
}
