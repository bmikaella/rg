package hr.fer.zemris.rg.canvas.elements.tri_dimensional


/**
 * Created by bmihaela.
 */
class ShapeShifter(planesToDraw: MutableList<Plane3D>, val rotation: Point3D = Point3D(0f, 0f, 1f)) {
    val planesToDraw: MutableList<Plane3D> = mutableListOf()
    init {
        this.planesToDraw.addAll(planesToDraw)
    }
}