package hr.fer.zemris.rg.canvas.descriptor

import hr.fer.zemris.rg.canvas.elements.tri_dimensional.BSpline
import hr.fer.zemris.rg.canvas.elements.tri_dimensional.Line3D
import hr.fer.zemris.rg.canvas.elements.tri_dimensional.Point3D
import org.apache.commons.math3.linear.MatrixUtils
import org.apache.commons.math3.linear.RealMatrix
import java.nio.file.Files
import java.nio.file.Path

/**
 * Created by bmihaela.
 */
class BsplineDescriptor(description: Path) {
    val point: MutableList<Point3D> = mutableListOf()
    val B = MatrixUtils.createRealMatrix(4, 4)
    val BDerived = MatrixUtils.createRealMatrix(3, 4)

    init {
        loadDescriptor(description)
        B.setRow(0, doubleArrayOf(-1.0, 3.0, -3.0, 1.0))
        B.setRow(1, doubleArrayOf(3.0, -6.0, 3.0, 0.0))
        B.setRow(2, doubleArrayOf(-3.0, 0.0, 3.0, 0.0))
        B.setRow(3, doubleArrayOf(1.0, 4.0, 1.0, 0.0))

        BDerived.setRow(0, doubleArrayOf(-1.0, 3.0, -3.0, 1.0))
        BDerived.setRow(1, doubleArrayOf(2.0, -4.0, 2.0, 0.0))
        BDerived.setRow(2, doubleArrayOf(-1.0, 0.0, 1.0, 0.0))
    }

    private fun loadDescriptor(description: Path) {
        val lines = Files.readAllLines(description)
        for (line in lines) {
            val elements = line.split(" ")
            point.add(Point3D(elements[0].toFloat(), elements[1].toFloat(), elements[2].toFloat()))
        }
    }

    fun createNewBspline(): BSpline {
        val segmentCount = point.size - 3
        val bSpline = BSpline()
        val directions = mutableListOf<Point3D>()
        for (segment in 1..segmentCount) {
            bSpline.addSegment(calcualteSegment(segment))
            directions.addAll(calculateDirections(segment))
        }
        bSpline.addDirections(directions)
        bSpline.addTangents(calculateTangents(directions, bSpline.splineDots))

        return bSpline
    }

    private fun calculateTangents(directions: MutableList<Point3D>, splineDots: MutableList<Point3D>): MutableList<Line3D> {
        val tangents = mutableListOf<Line3D>()
        for(i in 0 until directions.size){
            tangents.add(Line3D(splineDots.get(i), splineDots.get(i).add(directions.get(i)).scale(0.5f)))
        }
        return tangents
    }

    private fun calculateDirections(segment: Int): MutableList<Point3D> {
        var t: Double = 0.0
        var T: RealMatrix? = null
        var R: RealMatrix? = null
        var directionArrayInSegment: RealMatrix? = null
        var directionInSegment: Point3D? = null
        val directionsInSegment = mutableListOf<Point3D>()
        for (i in 0..99) {
            t = i / 100.0
            T = MatrixUtils.createRowRealMatrix(doubleArrayOf(Math.pow(t, 2.0), t, 1.0, 0.0))
            R = MatrixUtils.createRealMatrix(4, 3)
            R.setRow(0, point.get(segment - 1).getAsDoubleArray())
            R.setRow(1, point.get(segment).getAsDoubleArray())
            R.setRow(2, point.get(segment + 1).getAsDoubleArray())
            R.setRow(3, point.get(segment + 2).getAsDoubleArray())

            directionArrayInSegment = R.preMultiply(B).preMultiply(T.scalarMultiply(1.0 / 2.0))


            directionInSegment = Point3D(directionArrayInSegment.getColumn(0)[0].toFloat(),
                    directionArrayInSegment.getColumn(1)[0].toFloat(),
                    directionArrayInSegment.getColumn(2)[0].toFloat())

            directionsInSegment.add(directionInSegment)
        }

        return directionsInSegment
    }

    private fun calcualteSegment(segment: Int): MutableList<Point3D> {
        var t: Double = 0.0
        var T: RealMatrix? = null
        var R: RealMatrix? = null
        var dotArrayInSegment: RealMatrix? = null
        var dotInSegment: Point3D? = null
        val dotsInSegment = mutableListOf<Point3D>()
        for (i in 0..99) {
            t = i / 100.0
            T = MatrixUtils.createRowRealMatrix(doubleArrayOf(Math.pow(t, 3.0), Math.pow(t, 2.0), t * 1.0, 1.0))
            R = MatrixUtils.createRealMatrix(4, 3)
            R.setRow(0, point.get(segment - 1).getAsDoubleArray())
            R.setRow(1, point.get(segment).getAsDoubleArray())
            R.setRow(2, point.get(segment + 1).getAsDoubleArray())
            R.setRow(3, point.get(segment + 2).getAsDoubleArray())
            dotArrayInSegment = R.preMultiply(B).preMultiply(T.scalarMultiply(1.0 / 6))


            dotInSegment = Point3D(dotArrayInSegment.getColumn(0)[0].toFloat(),
                    dotArrayInSegment.getColumn(1)[0].toFloat(),
                    dotArrayInSegment.getColumn(2)[0].toFloat())
            dotsInSegment.add(dotInSegment)
        }
        return dotsInSegment
    }
}