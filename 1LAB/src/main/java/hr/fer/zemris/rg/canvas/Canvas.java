package hr.fer.zemris.rg.canvas;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import hr.fer.zemris.rg.canvas.elements.tri_dimensional.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;

/**
 * Created by bmihaela.
 */
public class Canvas {
	private BSpline bSpline;
	private ShapeShifter shapeShifter;
	GLCanvas glCanvas;
	JFrame jFrame;
	private Integer positionIndex = 0;

	public Canvas(BSpline bSpline, ShapeShifter shapeShifter) {
		this.bSpline = bSpline;
		this.shapeShifter = shapeShifter;
	}

	static {
		GLProfile.initSingleton();
	}

	public void movePositionForward() {
		if (positionIndex < bSpline.getPositionCount() - 1) {
			positionIndex++;
		}
	}

	public void movePositionBackward() {
		if (positionIndex > 0) {
			positionIndex--;
		}
	}

	public void redraw() {
		glCanvas.display();
	}

	public void addCanvasListener(KeyAdapter listener) {
		glCanvas.addKeyListener(listener);
	}

	public void makePanel(boolean randomColor)
			throws InvocationTargetException, InterruptedException {
		SwingUtilities.invokeAndWait(() -> {
			GLProfile glProfile = GLProfile.getDefault();
			GLCapabilities glCapabilities = new GLCapabilities(glProfile);
			glCanvas = new GLCanvas(glCapabilities);

			glCanvas.addGLEventListener(new GLEventListener() {
				@Override
				public void init(GLAutoDrawable glAutoDrawable) {

				}

				@Override
				public void dispose(GLAutoDrawable glAutoDrawable) {

				}

				@Override
				public void display(GLAutoDrawable glAutoDrawable) {
					GL2 gl2 = glAutoDrawable.getGL().getGL2();
					gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
					gl2.glLoadIdentity();
					//Translat so it can be seen
					gl2.glTranslatef(-10f, -15f, -70f);

					Random random = new Random();

					//bSplines
					java.util.List<java.util.List<Point3D>> segments = bSpline.getSegments();
					gl2.glBegin(GL.GL_LINE_STRIP);
					for (java.util.List<Point3D> segment : segments) {
						gl2.glColor3f(random.nextFloat(), random.nextFloat(), random.nextFloat());
						for (Point3D dot : segment) {
							gl2.glVertex3f(dot.getXCoordinate(), dot.getYCoordinate(), dot.getZCoordinate());
						}
					}
					gl2.glEnd();

					//Tangent drawing
					Line3D tangent = bSpline.getTangent(positionIndex);
					gl2.glColor3f(1f, 0f, 0f);
					gl2.glBegin(GL.GL_LINE_STRIP);
					gl2.glVertex3f(tangent.getBeginDot().getXCoordinate(), tangent.getBeginDot().getYCoordinate(),
							tangent.getBeginDot().getZCoordinate());
					gl2.glVertex3f(tangent.getEndDot().getXCoordinate(), tangent.getEndDot().getYCoordinate(),
							tangent.getEndDot().getZCoordinate());
					gl2.glEnd();


					//translate the object in consideration to the curve
					Point3D translation = bSpline.getTranslation(positionIndex);
					gl2.glTranslatef(translation.getXCoordinate(), translation.getYCoordinate(),
							translation.getZCoordinate());

					//rotate the object

					Point3D s = shapeShifter.getRotation();
					Point3D e = bSpline.getDirection(positionIndex);
					double xCoordinate = s.getYCoordinate()*e.getZCoordinate() - e.getYCoordinate()*s.getZCoordinate();
					double yCoordinate = - (s.getXCoordinate()*e.getZCoordinate() - e.getXCoordinate()*s.getZCoordinate());
					double zCoordinate = s.getXCoordinate()*e.getYCoordinate() - s.getYCoordinate()*e.getXCoordinate();

					double se = s.multiply(e);
					double sAps = Math.pow((Math.pow(s.getXCoordinate(), 2.0) + Math.pow(s.getYCoordinate(), 2.0) +
							Math.pow(s.getZCoordinate(), 2.0)), 0.5);
					double eAps = Math.pow((Math.pow(e.getXCoordinate(), 2.0) + Math.pow(e.getYCoordinate(), 2.0) +
							Math.pow(e.getZCoordinate(), 2.0)), 0.5);
					double angle = Math.acos(se/(sAps*eAps)) *360 / (2*Math.PI) ;

					gl2.glRotated(angle, xCoordinate,yCoordinate,zCoordinate);
					//ShapeShifter Drawing
					gl2.glColor3f(0.5f, 1f, 0.5f);
					for (Plane3D triangle : shapeShifter.getPlanesToDraw()) {
						if (randomColor) {
							gl2.glColor3f(random.nextFloat(), random.nextFloat(), random.nextFloat());
						}

						gl2.glBegin(GL.GL_LINE_STRIP);
						gl2.glVertex3f(triangle.getFirstPoint().getXCoordinate(),
								triangle.getFirstPoint().getYCoordinate(),
								triangle.getFirstPoint().getZCoordinate());
						gl2.glVertex3f(triangle.getSecondPoint().getXCoordinate(),
								triangle.getSecondPoint().getYCoordinate(),
								triangle.getSecondPoint().getZCoordinate());
						gl2.glEnd();


						gl2.glBegin(GL.GL_LINE_STRIP);
						gl2.glVertex3f(triangle.getSecondPoint().getXCoordinate(),
								triangle.getSecondPoint().getYCoordinate(),
								triangle.getSecondPoint().getZCoordinate());
						gl2.glVertex3f(triangle.getThirdPoint().getXCoordinate(),
								triangle.getThirdPoint().getYCoordinate(),
								triangle.getThirdPoint().getZCoordinate());
						gl2.glEnd();


						gl2.glBegin(GL.GL_LINE_STRIP);
						gl2.glVertex3f(triangle.getThirdPoint().getXCoordinate(),
								triangle.getThirdPoint().getYCoordinate(),
								triangle.getThirdPoint().getZCoordinate());
						gl2.glVertex3f(triangle.getFirstPoint().getXCoordinate(),
								triangle.getFirstPoint().getYCoordinate(),
								triangle.getFirstPoint().getZCoordinate());
						gl2.glEnd();

					}


				}


				@Override
				public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
					GL2 gl2 = glAutoDrawable.getGL().getGL2();
					gl2.glViewport(0, 0, width, height);
					gl2.glMatrixMode(GL2.GL_PROJECTION);
					gl2.glLoadIdentity();

					GLU glu = new GLU();

					glu.gluPerspective(50f, width * 1.0 / height, 0.1f, 150f);

					gl2.glMatrixMode(GL2.GL_MODELVIEW);
					gl2.glLoadIdentity();
//
				}
			});

			jFrame = new JFrame("Crtanje");
			jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			jFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					jFrame.dispose();
					System.exit(0);
				}
			});
			jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
			jFrame.setSize(640, 480);
			jFrame.setVisible(true);
			glCanvas.requestFocusInWindow();
		});
	}
}
