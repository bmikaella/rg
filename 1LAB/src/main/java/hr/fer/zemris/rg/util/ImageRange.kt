package hr.fer.zemris.rg.util

/**
 * Created by bmihaela.
 */
class ImageRange(
        var maxXCoordinare: Float = 0f,
        var maxYCoordinare: Float = 0f,
        var minXCoordinare: Float = 0f,
        var minYCoordinare: Float = 0f)