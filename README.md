## 3Lab prica postanka

Svaki dan plavi kvadrati prelaze s jedne strane na drugu. Nekoć davno je vladao sklad njihovim svijetom ali danas put s jedne strane na drugu im može donijeti smrt.  Kako bi sada sigurno otišli na drugu stranu moraju proći područje koje zovu zona smrti. Zona smrti ne postoji dugo. Ona tek od nedavno ugrožava život plavih kvadrata. Ili tako barem oni misle.


Jedan plavi kvadrat se pitao i pitao svaki dan zašto mora prelaziti s jedne strane na drugu. Ali mu nitko nije htio reći zašto. Vjerojatno ni sami nisu bili svjesni zašto. I tako je odlučio promijeniti svoj pogled na život. On se zarotirao. Ali nakon rotacije shvatio je da više nije kompatibilan s tim svijetom. Rubovi im se više nisu poklapali. Lutao je u nadi i shvatiti novo znanje koje mu je omogućila promjena perspektive. Promijenio se. Postao je crveni.


Crveni kvadrat sada luta u nadi da će još nekome podariti to znanje. Svaki dan ide od plavog kvadrata do plavog kvadrata te majeutikom pokušava ukazati na istinu ali to je previše za plave kvadrate pa oni jednostavno iščeznu iz ovog života. Crveni kvadrat je tako osuđen ići od plavog do plavog kvadrata dok ne pokuša svima podariti znanje ili dok ne bude toliko iscrpljen i uništen odgovorima plavih kvadrata.